<?php

/*==================================================
=            Starter Theme Introduction            =
==================================================*/

/**
 *
 * About Starter
 * --------------
 * Starter is a project by Calvin Koepke to create a starter theme for Genesis Framework developers that doesn't over-bloat
 * their starting base. It includes commonly used templates, codes, and styles, along with optional SCSS and Gulp tasking.
 *
 * Credits and Licensing
 * --------------
 * Starter was created by Calvin Koepke, and is under GPL 2.0+.
 *
 * Find me on Twitter: @cjkoepke
 *
 */


/*============================================
=            Begin Functions File            =
============================================*/

/**
 *
 * Define Child Theme Constants
 *
 * @since 1.0.0
 *
 */
define( 'CHILD_THEME_NAME', 'Starter Theme' );
define( 'CHILD_THEME_AUTHOR', 'Calvin Koepke' );
define( 'CHILD_THEME_AUTHOR_URL', 'https://calvinkoepke.com/' );
define( 'CHILD_THEME_URL', 'http://startertheme.io' );
define( 'CHILD_THEME_VERSION', '1.0.0' );
define( 'TEXT_DOMAIN', 'startertheme' );

//* Start The Engine
include_once( get_template_directory() . '/lib/init.php');



//* Load Assets
include get_stylesheet_directory() . '/lib/load-assets.php';
//* Theme Supports
include get_stylesheet_directory() . '/lib/theme-supports.php';
//* Apply Custom Body Classes
include_once( get_stylesheet_directory() . '/lib/classes.php' );
//* Apply Starter Defaults (Overides Genesis Defaults)
include_once( get_stylesheet_directory() . '/lib/defaults.php' );
//* Apply Default Starter Attributes
include_once( get_stylesheet_directory() . '/lib/attributes.php' );
