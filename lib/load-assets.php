<?php

/**
 *
 * Load files in the /assets/ directory
 *
 * @since 1.0.0
 *
 */
add_action( 'wp_enqueue_scripts', 'startertheme_load_assets' );
function startertheme_load_assets() {

	/* Load Google Font */
	wp_enqueue_style( 'startertheme-fonts', '//fonts.googleapis.com/css?family=Lato:400,700,700italic', array(), CHILD_THEME_VERSION );

	/* Load JS */
	wp_enqueue_script( 'startertheme-global', get_stylesheet_directory_uri() . '/assets/js/global.js', array( 'jquery' ), CHILD_THEME_VERSION, true );
	wp_enqueue_script( 'startertheme-responsive-menu', get_stylesheet_directory_uri() . '/assets/js/responsive-menu.js', array( 'jquery', 'startertheme-global' ), CHILD_THEME_VERSION, true );

	/* Load Icons */
	wp_enqueue_style( 'dashicons' );

	/* Localize Responsive Menu Variables */
	$output = array(
		'mainMenu' => __( 'Menu', 'startertheme' ),
		'subMenu'  => __( 'Menu', 'startertheme' ),
	);
	wp_localize_script( 'startertheme-responsive-menu', 'starterthemeL10n', $output );

}
